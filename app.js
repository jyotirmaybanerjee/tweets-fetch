var express = require('express');
var Twitter = require('mtwitter');
var errorHandler = require('errorhandler');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/dist'));
app.use(errorHandler());
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!' + err.stack);
});

var port = process.env.PORT || 3000;
var router = express.Router();

var twitter = new Twitter({
    consumer_key: '1HV9OtMunqfE0o2Ki5ejcArCJ',
    consumer_secret: '0Mxy453t964d7Lb1GuXdFkrNgZsQ9FxHLYBobe09mU4j7n6Xdh',
    access_token_key: '3103743840-bG5axUarJZC88v7SlpakFkfqkIc8m4t3GFfRW7P',
    access_token_secret: 'NlcxUubxm1zXSnNsvOY9zzSxBI0W78uOikV1ORXfo6C5v'
});

router.get('/timeLineTweets/:item/:count', function(req, res) {

    var item = req.params.item;
    var count = req.params.count;
    twitter.get('statuses/user_timeline', {
        screen_name: item,
        count: count
    }, function(err, item) {
        if (err) console.log(err);
        res.send(item);
    });
});

app.get('/', function(req, res) {

    res.render('index.html');
});

app.use('/api', router);

app.listen(port);
console.log('Server started on ' + port);