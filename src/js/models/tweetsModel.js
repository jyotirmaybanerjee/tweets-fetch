var $ = require('jquery');
var Backbone = require('backbone');

Backbone.$ = $;

var Model = Backbone.Model.extend({

    urlRoot: '',
    initialize: function(options) {}

});

module.exports = Model;