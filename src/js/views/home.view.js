var $ = require('jquery');
var Backbone = require('backbone');
var TweetView = require('../views/tweet.view');
var template = require('../templates/home.template.hbs');
var LocalStorage = require("backbone.localstorage");

Backbone.$ = $;

var HomeView = Backbone.View.extend({
  el: '#main',
  template: template,
  localStorage: new LocalStorage('appDirectTweets'),

  events: {
  	"change #no-of-tweets" : "updateLocalStorage"
  },

  initialize: function(options) {
  	if(!localStorage.getItem('tweetCount')) {
  		localStorage.setItem('tweetCount', 30);
  	}
  },

  render: function() {
    this.$el.html(template());
  	$('#no-of-tweets').val(localStorage.getItem('tweetCount'));
    new TweetView({"el": "#appdirect-tweets", "component": "AppDirect", "tweetCount": localStorage.getItem('tweetCount')});
    new TweetView({"el": "#laughingsquid-tweets", "component": "laughingsquid", "tweetCount": localStorage.getItem('tweetCount')});
    new TweetView({"el": "#techcrunch-tweets", "component": "techcrunch", "tweetCount": localStorage.getItem('tweetCount')});
  },

  updateLocalStorage: function() {

  	var self = this;
  	var newCount = parseInt($('#no-of-tweets').val(), 10);
  	localStorage.setItem('tweetCount', newCount);
  	self.render();
  }
});

module.exports = HomeView;
