var $ = require('jquery');
var Backbone = require('backbone');
var template = require('../templates/tweets.template.hbs');
var Model = require('../models/tweetsModel');

var Handlebars = require("hbsfy/runtime");

Handlebars.registerHelper("formatUrl", function(str) {

    var replacedText, replacePattern1, replacePattern2;
	replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = str.replace(replacePattern1, '<small><a href="$1" target="_blank">$1</a></small>');

    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '<small>$1<a href="http://$2" target="_blank">$2</a></small>');

	var output = '<strong>'+replacedText+'</strong>';
    return new Handlebars.SafeString(output);

});

Handlebars.registerHelper("formatDate", function(string) {
	var dt = new Date(string);
    return dt.getMonth()+1 + "/" + dt.getDate() + "/" + dt.getFullYear();
});

Backbone.$ = $;

var TweetView = Backbone.View.extend({
    el: '',
    template: template,

    initialize: function(options) {

        var self = this;
        self.el = options.el;
        var model = new Model();
        model.urlRoot = '/api/timeLineTweets/' + options.component + '/' + options.tweetCount;
        model.fetch({
            success: function(results) {
                self.render(results);
            },
            error: function(xhr, statusString, errorThrown) {
                console.log('error- ', xhr);
            }
        });
    },

    render: function(tweets) {
        console.log('tweets- ', tweets.toJSON());
        this.$el.html(this.template({
            "list": tweets.toJSON()
        }));
    }
});

module.exports = TweetView;